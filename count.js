document.getElementById("countButton").onclick = function () {
    // teu código vai aqui ... 

    let typedText = document.getElementById("textInput").value;

    typedText = typedText.toLowerCase();
    // // Isto muda todas as letras para minúsculas
    typedText = typedText.replace(/[^a-z'\s]+/g, "");
    // Isso se livra de todos os caracteres exceto letras comuns, espaços e apóstrofos.
    // Iremos aprender mais sobre como usar a função replace numa lição mais à frente.

    let letterCounts = {};
    for (let i = 0; i < typedText.length; i++) {
        currentLetter = typedText[i];

        if (letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1;
        } else {
            letterCounts[currentLetter]++;
        }

    }

    for (let letter in letterCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
        span.appendChild(textContent);
        document.getElementById("lettersDiv").appendChild(span);
    }
    

    words = typedText.split(/\s/);

    let wordsDiv = {};
    for (let i = 0; i < words.length; i++) {
        currentwords = words[i];

        if (wordsDiv[currentwords] === undefined) {
            wordsDiv[currentwords] = 1;
        } else {
            wordsDiv[currentwords]++;
        }

    }

    for (let words in wordsDiv) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + words + "\": " + wordsDiv[words] + ", ");
        span.appendChild(textContent);
        document.getElementById("wordsDiv").appendChild(span);
    }

}
